#include "mainw.h"
#include "ui_mainw.h"
#include "ui_diagram.h"

MainW::MainW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainW),lol(new Ui::diagramform)
{
    ui->setupUi(this);

    newwindow = new QWidget(0);
    lol->setupUi(newwindow);

    newwindow->show();



}

MainW::~MainW()
{
    delete ui;
}

paTestData * MainW::Load_Gui_Datas(){

    paTestData  * tempdat  = (paTestData * )malloc(sizeof( paTestData));

    QString samprate = ui->sinw_right_samp_rate_edit->text() ;
    QString outputF =  ui->sinw_right_outputfile_edit->text();
    QString time =  ui->sinw_right_time_edit->text();
    QString left_freq =   ui->sinw_lay_R_LFREQ_edit->text();
    QString right_freq =  ui->sinw_lay_R_RFREQ_edit->text();
    QString chan = ui->sinw_right_chan_cobox->currentText();


    tempdat-> SAMPLE_RATE = samprate.toInt();
    tempdat-> SAMPLE_COUNT = (tempdat-> SAMPLE_RATE * time.toInt());
    tempdat-> CHANNELS = chan.toInt();
    tempdat-> AMPLITUDE = ( 1.0 * 0x7F000000);
    tempdat-> PLAYTIME = time.toInt();

    tempdat-> OFILE = NULL;
    //tempdat-> OFILE = (char *) malloc(200);

    std::string s = outputF.toStdString();
    const char * p = s.c_str();


    char *string =  const_cast<char*> ( s.c_str() );

    char *copystr;
   if(allocstr(strlen(string), &tempdat-> OFILE)){
       strcpy(tempdat-> OFILE, string);
   }
   else {
       fprintf(stderr, "out of memory\n");

   }

    //QByteArray array = outputF.toLocal8Bit();

    tempdat-> PASAFO = GetSampleFormatFromGui();

    if (tempdat->CHANNELS == 2)
    {
        tempdat->LEFT_FREQ = ( left_freq.toFloat() / tempdat->SAMPLE_RATE );
        tempdat->RIGHT_FREQ = ( right_freq.toFloat() / tempdat->SAMPLE_RATE );

    }else{
        tempdat->LEFT_FREQ = ( left_freq.toFloat() / tempdat->SAMPLE_RATE );
    }

    QString tab = ui->tabWidget->tabText(ui->tabWidget->currentIndex());

    if(tab == QString("Sin wave")){
        tempdat->type = "sin\0";
    }else if (chan == QString("Saw wave")){
        tempdat->type = "saw\0";
    }


    return tempdat;

}


