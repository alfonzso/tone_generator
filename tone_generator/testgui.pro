#-------------------------------------------------
#
# Project created by QtCreator 2014-03-27T12:07:25
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = testgui
TEMPLATE = app


SOURCES += main.cpp\
        mainw.cpp \
    mainw_gui_func.cpp \
    c_functions.cpp

HEADERS  += mainw.h \
    c_functions.h

FORMS    += mainw.ui \
    diagram.ui

LIBS += -L$$PWD/SOUND_LIB/lib/ -llibportaudio
INCLUDEPATH += $$PWD/SOUND_LIB/include
DEPENDPATH += $$PWD/SOUND_LIB/include


LIBS += -L$$PWD/SOUND_LIB/lib/ -lsndfile
INCLUDEPATH += $$PWD/SOUND_LIB/include
DEPENDPATH += $$PWD/SOUND_LIB/include
