#include <mainw.h>
#include "ui_mainw.h"

void MainW::on_sinw_right_chan_cobox_currentIndexChanged(const QString &arg1)
{
    if (arg1 == QString("1"))
    {
        this->ui->sinw_lay_R_RFREQ_edit->setEnabled(false);

    }else if ( arg1 == QString("2")){
        ui->sinw_lay_R_LFREQ_edit->setEnabled(true);
        ui->sinw_lay_R_RFREQ_edit->setEnabled(true);
    }
}

int MainW::on_LoadNPlayFile_Button_clicked()
{

    int err = 0;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "",
                                                    tr("Files (*.wav)"));

    fileName =  QDir::toNativeSeparators (fileName ) ;

    if ( (err = Load_And_Play_Files(fileName.toUtf8().constData())) != 0 ){
        qDebug()<<"Error happened !";
    }

    return 0;
}

int MainW::GetSampleFormatFromGui(){

    QString temp = ui->sinw_right_sampform_cobox->currentText();

    if ( temp == QString("Float32") ){
        return 0x00000001;
    }else if (temp == QString("Int32")){
        return 0x00000002;
    }else if (temp == QString("Int24")){
        return 0x00000004;
    }else if (temp == QString("Int16")){
        return 0x00000008;
    }else if (temp == QString("Int8")){
        return 0x00000010;
    }else if (temp == QString("UInt8")){
        return 0x00000020;
    }

    return 1;
}

void MainW::on_sine_play_wave_button_clicked()
{
    paTestData * datdata = ( paTestData * )malloc(sizeof( paTestData));

    datdata = Load_Gui_Datas();

    Play_Tone_Now(datdata);
}

void MainW::on_sine_save_as_button_clicked()
{
    paTestData * datdata = ( paTestData * )malloc(sizeof( paTestData));

    datdata = Load_Gui_Datas();

    MakeSinToFile(datdata);
}

int MainW::MakeSinToFile(paTestData * dat){

    SNDFILE *file ; SF_INFO sfinfo ; int k; double *buffer ;

    //if (! (buffer = (int*)malloc (2 * dat->SAMPLE_COUNT * sizeof (int))))
    if (! (buffer = (double*)malloc (2 * dat->SAMPLE_COUNT * sizeof (double))))
    {
        printf ("Malloc failed.\n") ;
        exit (0) ;
    }

    memset (&sfinfo, 0, sizeof (sfinfo)) ;
    sfinfo.samplerate	= dat->SAMPLE_RATE ;
    sfinfo.frames		= dat->SAMPLE_COUNT ;
    sfinfo.channels		= dat->CHANNELS ;
    sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_24) ;


    dat->AMPLITUDE = 1.0;

    if (! (file = sf_open (dat->OFILE, SFM_WRITE, &sfinfo)))
    {	printf ("Error : Not able to open output file.\n") ;
        return 1 ;
    }

    if (sfinfo.channels == 1)
    {
        for (k = 0 ; k < dat->SAMPLE_COUNT ; k++)
        {
            buffer [k] = dat->AMPLITUDE * sin (dat->LEFT_FREQ * 2 * k * M_PI) ;
        }
    }
    else if (sfinfo.channels == 2)
    {	for (k = 0 ; k < dat->SAMPLE_COUNT ; k++)
        {
            buffer [2 * k] = dat->AMPLITUDE * sin (dat->LEFT_FREQ * 2 * k * M_PI) ;

            buffer [2 * k + 1] = dat->AMPLITUDE * sin (dat->RIGHT_FREQ * 2 * k * M_PI) ;
        }
    }
    else
    {	printf ("makesine can only generate mono or stereo files.\n") ;
        exit (1) ;
    } ;

    double quality ;
    quality = 0.1;

    // not working, only OGA files use this command
    //sf_command(file, SFC_SET_VBR_ENCODING_QUALITY, &quality, sizeof(double));

    if (
            sf_write_double(file, buffer, sfinfo.channels * dat->SAMPLE_COUNT) !=  (sfinfo.channels * dat->SAMPLE_COUNT)
       )
    {
        puts (sf_strerror (file)) ;
    }

    sf_close (file) ;

    return 0;
}

/*

int MainW::MakeSinToFile(paTestData * dat){

    SNDFILE *file ; SF_INFO sfinfo ; int k; int *buffer ;

    if (! (buffer = (int*)malloc (2 * dat->SAMPLE_COUNT * sizeof (int))))
    {
        printf ("Malloc failed.\n") ;
        exit (0) ;
    }

    memset (&sfinfo, 0, sizeof (sfinfo)) ;
    sfinfo.samplerate	= dat->SAMPLE_RATE ;
    sfinfo.frames		= dat->SAMPLE_COUNT ;
    sfinfo.channels		= dat->CHANNELS ;
    sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_24) ;


    if (! (file = sf_open (dat->OFILE, SFM_WRITE, &sfinfo)))
    {	printf ("Error : Not able to open output file.\n") ;
        return 1 ;
    }

    if (sfinfo.channels == 1)
    {
        for (k = 0 ; k < dat->SAMPLE_COUNT ; k++)
        {
            buffer [k] = dat->AMPLITUDE * sin (dat->LEFT_FREQ * 2 * k * M_PI) ;
        }
    }
    else if (sfinfo.channels == 2)
    {	for (k = 0 ; k < dat->SAMPLE_COUNT ; k++)
        {
            buffer [2 * k] = dat->AMPLITUDE * sin (dat->LEFT_FREQ * 2 * k * M_PI) ;
            buffer [2 * k + 1] = dat->AMPLITUDE * sin (dat->RIGHT_FREQ * 2 * k * M_PI) ;
        }
    }
    else
    {	printf ("makesine can only generate mono or stereo files.\n") ;
        exit (1) ;
    } ;

    sf_command(STA(outfile), SFC_SET_VBR_ENCODING_QUALITY, &O->quality, sizeof(double));


    if (sf_write_int (file, buffer, sfinfo.channels * dat->SAMPLE_COUNT) !=  sfinfo.channels * dat->SAMPLE_COUNT)
    {
        puts (sf_strerror (file)) ;
    }

    sf_close (file) ;

    return 0;
}

*/
