#ifndef CHEADERFILE_H
#define CHEADERFILE_H

////////////////////////////////////////////////
////////////////SOUND GENERATOR/////////////////
////////////////////////////////////////////////

#include	<stdio.h>
#include	<stdlib.h>
#include    <string>
#include	<math.h>
#include    <strings.h>

#include	<sndfile.h>
#include    <portaudio.h>

#ifndef		M_PI
#define		M_PI		3.14159265358979323846264338
#endif

struct OurData
{
    SNDFILE *sndFile;
    SF_INFO sfInfo;
    int position;
};


typedef struct
{
    int SAMPLE_RATE;
    int SAMPLE_COUNT;
    int CHANNELS;
    float AMPLITUDE;
    float LEFT_FREQ;
    float RIGHT_FREQ;
    int PLAYTIME;
    int PASAFO;

    char * OFILE;
    char * type;

}paTestData;

int allocstr(int len, char **retptr);

int LoadFileCallback(const void *input,
                     void *output,
                     unsigned long frameCount,
                     const PaStreamCallbackTimeInfo* paTimeInfo,
                     PaStreamCallbackFlags statusFlags,
                     void *userData);

int PlaySinCallback( const void *inputBuffer, void *outputBuffer,
                     unsigned long framesPerBuffer,
                     const PaStreamCallbackTimeInfo* timeInfo,
                     PaStreamCallbackFlags statusFlags,
                     void *userData );


int Load_And_Play_Files(const char *str);

int Play_Tone_Now(paTestData *dat);

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////


#endif // CHEADERFILE_H

