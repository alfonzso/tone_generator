
#include <mainw.h>
#include <ui_mainw.h>

int allocstr(int len, char **retptr)
{
    char *p = (char *) malloc(len + 1);	/* +1 for \0 */
    if(p == NULL)
        return 0;
    *retptr = p;
    return 1;
}


int LoadFileCallback(const void *input,
                     void *output,
                     unsigned long frameCount,
                     const PaStreamCallbackTimeInfo* paTimeInfo,
                     PaStreamCallbackFlags statusFlags,
                     void *userData)
{
    /* we passed a data structure into the callback so we have something to work with */
    struct OurData *data = (struct OurData *)userData; int *cursor; /* current pointer into the output  */
    int *out = (int *)output;
    int thisSize = frameCount;
    int thisRead;

    (void) input;
    (void) paTimeInfo;
    (void) statusFlags;

    cursor = out; /* set the output cursor to the beginning */
    while (thisSize > 0)
    {
        /* seek to our current file position */
        sf_seek(data->sndFile, data->position, SEEK_SET);

        /* are we going to read past the end of the file?*/
        if (thisSize > (data->sfInfo.frames - data->position))
        {
            /*if we are, only read to the end of the file*/
            thisRead = data->sfInfo.frames - data->position;
            /* and then loop to the beginning of the file */
            data->position = 0;
        }
        else
        {
            /* otherwise, we'll just fill up the rest of the output buffer */
            thisRead = thisSize;
            /* and increment the file position */
            data->position += thisRead;
        }

        /* since our output format and channel interleaving is the same as
        sf_readf_int's requirements */
        /* we'll just read straight into the output buffer */
        sf_readf_int(data->sndFile, cursor, thisRead);
        /* increment the output cursor*/
        cursor += thisRead;
        /* decrement the number of samples left to process */
        thisSize -= thisRead;
    }

    return paContinue;
}

int PlaySinCallback( const void *inputBuffer, void *outputBuffer,
                     unsigned long framesPerBuffer,
                     const PaStreamCallbackTimeInfo* timeInfo,
                     PaStreamCallbackFlags statusFlags,
                     void *userData )
{
    /* Cast data passed through stream to our structure. */
    paTestData *data = (paTestData*)userData;
    float *out = (float*)outputBuffer;
    (void) timeInfo; /* Prevent unused variable warnings. */
    (void) statusFlags;
    (void) inputBuffer;

    float LEFT_FREQ = data->LEFT_FREQ;
    float RIGHT_FREQ = data->RIGHT_FREQ;

    float sawWaveL = 0.0;
    float sawWaveR = 0.0;
    float sinwaveL =  0.0;
    float sinwaveR =  0.0;

    static unsigned long n = 0;
    //if you dont use 'n' var, that sound become noisy
    for(unsigned long i = 0; i < framesPerBuffer; i++,n++)
    {
        if (data->CHANNELS == 2){

            if (strncmp(data->type,"sin",100) == 0){

                sinwaveL =  (float) ( 1.0 * sin ( fmod( LEFT_FREQ * n , 1.0) * 2 * M_PI ));
                sinwaveR =  (float) ( 1.0 * sin ( fmod( RIGHT_FREQ * n , 1.0) * 2 * M_PI ));
                *out++ = sinwaveL;
                *out++ = sinwaveR;

            }else if (strncmp(data->type,"saw",100) == 0){

                sawWaveL =  (float) ( fmod( LEFT_FREQ * n , 1.0) * 2 - 1 );
                sawWaveR =  (float) ( fmod( RIGHT_FREQ * n , 1.0) * 2 - 1 );

                *out++ = sawWaveL;
                *out++ = sawWaveR;
            }


        }else{

            if (strncmp(data->type,"sin",100) == 0){

                sinwaveL =  (float) ( 1.0 * sin ( fmod( LEFT_FREQ * n , 1.0) * 2 * M_PI));
                *out++ = sinwaveL;

            }else if (strncmp(data->type,"saw",100) == 0){
                sawWaveL =  (float) ( fmod( LEFT_FREQ * n , 1.0) * 2 - 1 );
                *out++ = sawWaveL;
            }

        }

    }

    return 0;
}

int Load_And_Play_Files(const char * str){

    struct OurData *data = ( OurData *)malloc(sizeof( OurData));
    PaStream *stream;
    PaError error;
    PaStreamParameters outputParameters;

    /* initialize our data structure */
    data->position = 0;
    data->sfInfo.format = 0;
    /* try to open the file */

    data->sndFile = sf_open( str, SFM_READ, &data->sfInfo);

    if (!data->sndFile)
    {
        qDebug()<<"Error  opening file !";
        return 1;
    }

    /* start portaudio */
    Pa_Initialize();

    /* set the output parameters */
    outputParameters.device = Pa_GetDefaultOutputDevice(); /* use the default device */
    outputParameters.channelCount = data->sfInfo.channels; /* use the same number of channels as our sound file */
    outputParameters.sampleFormat = paInt32; /* 32bit int format */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;//0.2; /* 200 ms ought to satisfy even the worst sound card */
    outputParameters.hostApiSpecificStreamInfo = 0; /* no api specific data */

    /* try to open the output */
    error = Pa_OpenStream(&stream,  /* stream is a 'token' that we needto save for future portaudio calls */
                          0,  /* no input */
                          &outputParameters,
                          data->sfInfo.samplerate,  /* use the same sample rate as the sound file */
                          256,//paFramesPerBufferUnspecified, //
                          paNoFlag,  /* no special modes (clip off, dither off) */
                          LoadFileCallback,  /* callback function defined above */
                          data ); /* pass in our data structure so the callback knows what's up */

    /* if we can't open it, then bail out */
    if (error)
    {
        qDebug()<<"Error  opening output, error code = " << error;
        Pa_Terminate();
        return 1;
    }

    /* when we start the stream, the callback starts getting called */
    Pa_StartStream(stream);
    Pa_Sleep((data->sfInfo.frames / data->sfInfo.samplerate) * 1000);
    Pa_StopStream(stream); // stop the stream
    Pa_Terminate(); // and shut down portaudio

    return 0;
}

int Play_Tone_Now( paTestData * dat){

    paTestData * datdata = ( paTestData * )malloc(sizeof( paTestData));

    datdata = dat;

    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err;

    err = Pa_Initialize();
    if( err != paNoError ) return 1;;

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
        fprintf(stderr,"Error: No default output device.\n");
        return 1;;
    }
    outputParameters.channelCount = datdata->CHANNELS;       /* stereo output */
    outputParameters.sampleFormat = (PaSampleFormat) datdata->PASAFO;// paFloat32;// /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = 0; /* no api specific data */

    err = Pa_OpenStream(
                &stream,
                NULL,
                &outputParameters,
                datdata->SAMPLE_RATE,
                256,//paFramesPerBufferUnspecified,//
                paNoFlag,
                PlaySinCallback,
                datdata );

    if (err)
    {
        printf("error opening output, error code = %i\n", err);
        Pa_Terminate();
        return 1;
    }
    Pa_StartStream(stream);

    Pa_Sleep(datdata->PLAYTIME * 1000); /* pause for 2 seconds (2000ms) so we can hear a bit of the output */
    Pa_StopStream(stream); // stop the stream
    Pa_Terminate(); // and shut down portaudio

    return 0;
}

