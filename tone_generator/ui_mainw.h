/********************************************************************************
** Form generated from reading UI file 'mainw.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINW_H
#define UI_MAINW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainW
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *LoadNPlayFile_Button;
    QTabWidget *tabWidget;
    QWidget *sinw_tab;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *sinw_tab_hor_lay_parent;
    QVBoxLayout *sinw_lay_left;
    QLabel *sinw_left_output_file_lab;
    QLabel *sinw_left_samp_rate_lab;
    QLabel *sinw_left_LNR_freq;
    QLabel *sinw_left_channels_lab;
    QLabel *sinw_left_sample_format_lab;
    QLabel *sinw_left_play_time_lab;
    QVBoxLayout *sinw_lay_right;
    QLineEdit *sinw_right_outputfile_edit;
    QLineEdit *sinw_right_samp_rate_edit;
    QVBoxLayout *sinw_lay_R_Freq_vert_lay;
    QHBoxLayout *sinw_lay_R_Freq_hort_lay;
    QLineEdit *sinw_lay_R_LFREQ_edit;
    QLineEdit *sinw_lay_R_RFREQ_edit;
    QComboBox *sinw_right_chan_cobox;
    QComboBox *sinw_right_sampform_cobox;
    QLineEdit *sinw_right_time_edit;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *sine_play_wave_button;
    QPushButton *sine_save_as_button;
    QSpacerItem *verticalSpacer;
    QWidget *saww_tab;

    void setupUi(QMainWindow *MainW)
    {
        if (MainW->objectName().isEmpty())
            MainW->setObjectName(QStringLiteral("MainW"));
        MainW->resize(400, 300);
        centralWidget = new QWidget(MainW);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(2);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(3, 3, 3, 3);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        LoadNPlayFile_Button = new QPushButton(centralWidget);
        LoadNPlayFile_Button->setObjectName(QStringLiteral("LoadNPlayFile_Button"));

        verticalLayout->addWidget(LoadNPlayFile_Button);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        sinw_tab = new QWidget();
        sinw_tab->setObjectName(QStringLiteral("sinw_tab"));
        verticalLayout_4 = new QVBoxLayout(sinw_tab);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        sinw_tab_hor_lay_parent = new QHBoxLayout();
        sinw_tab_hor_lay_parent->setSpacing(2);
        sinw_tab_hor_lay_parent->setObjectName(QStringLiteral("sinw_tab_hor_lay_parent"));
        sinw_tab_hor_lay_parent->setContentsMargins(1, 1, 1, 1);
        sinw_lay_left = new QVBoxLayout();
        sinw_lay_left->setSpacing(2);
        sinw_lay_left->setObjectName(QStringLiteral("sinw_lay_left"));
        sinw_lay_left->setContentsMargins(1, 1, 1, 1);
        sinw_left_output_file_lab = new QLabel(sinw_tab);
        sinw_left_output_file_lab->setObjectName(QStringLiteral("sinw_left_output_file_lab"));

        sinw_lay_left->addWidget(sinw_left_output_file_lab, 0, Qt::AlignHCenter);

        sinw_left_samp_rate_lab = new QLabel(sinw_tab);
        sinw_left_samp_rate_lab->setObjectName(QStringLiteral("sinw_left_samp_rate_lab"));

        sinw_lay_left->addWidget(sinw_left_samp_rate_lab, 0, Qt::AlignHCenter);

        sinw_left_LNR_freq = new QLabel(sinw_tab);
        sinw_left_LNR_freq->setObjectName(QStringLiteral("sinw_left_LNR_freq"));

        sinw_lay_left->addWidget(sinw_left_LNR_freq, 0, Qt::AlignHCenter);

        sinw_left_channels_lab = new QLabel(sinw_tab);
        sinw_left_channels_lab->setObjectName(QStringLiteral("sinw_left_channels_lab"));

        sinw_lay_left->addWidget(sinw_left_channels_lab, 0, Qt::AlignHCenter);

        sinw_left_sample_format_lab = new QLabel(sinw_tab);
        sinw_left_sample_format_lab->setObjectName(QStringLiteral("sinw_left_sample_format_lab"));

        sinw_lay_left->addWidget(sinw_left_sample_format_lab, 0, Qt::AlignHCenter|Qt::AlignVCenter);

        sinw_left_play_time_lab = new QLabel(sinw_tab);
        sinw_left_play_time_lab->setObjectName(QStringLiteral("sinw_left_play_time_lab"));

        sinw_lay_left->addWidget(sinw_left_play_time_lab, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        sinw_tab_hor_lay_parent->addLayout(sinw_lay_left);

        sinw_lay_right = new QVBoxLayout();
        sinw_lay_right->setSpacing(2);
        sinw_lay_right->setObjectName(QStringLiteral("sinw_lay_right"));
        sinw_lay_right->setContentsMargins(1, 1, 1, 1);
        sinw_right_outputfile_edit = new QLineEdit(sinw_tab);
        sinw_right_outputfile_edit->setObjectName(QStringLiteral("sinw_right_outputfile_edit"));

        sinw_lay_right->addWidget(sinw_right_outputfile_edit);

        sinw_right_samp_rate_edit = new QLineEdit(sinw_tab);
        sinw_right_samp_rate_edit->setObjectName(QStringLiteral("sinw_right_samp_rate_edit"));

        sinw_lay_right->addWidget(sinw_right_samp_rate_edit);

        sinw_lay_R_Freq_vert_lay = new QVBoxLayout();
        sinw_lay_R_Freq_vert_lay->setSpacing(0);
        sinw_lay_R_Freq_vert_lay->setObjectName(QStringLiteral("sinw_lay_R_Freq_vert_lay"));
        sinw_lay_R_Freq_hort_lay = new QHBoxLayout();
        sinw_lay_R_Freq_hort_lay->setSpacing(0);
        sinw_lay_R_Freq_hort_lay->setObjectName(QStringLiteral("sinw_lay_R_Freq_hort_lay"));
        sinw_lay_R_LFREQ_edit = new QLineEdit(sinw_tab);
        sinw_lay_R_LFREQ_edit->setObjectName(QStringLiteral("sinw_lay_R_LFREQ_edit"));
        sinw_lay_R_LFREQ_edit->setEnabled(true);
        sinw_lay_R_LFREQ_edit->setFrame(true);

        sinw_lay_R_Freq_hort_lay->addWidget(sinw_lay_R_LFREQ_edit);

        sinw_lay_R_RFREQ_edit = new QLineEdit(sinw_tab);
        sinw_lay_R_RFREQ_edit->setObjectName(QStringLiteral("sinw_lay_R_RFREQ_edit"));

        sinw_lay_R_Freq_hort_lay->addWidget(sinw_lay_R_RFREQ_edit);


        sinw_lay_R_Freq_vert_lay->addLayout(sinw_lay_R_Freq_hort_lay);


        sinw_lay_right->addLayout(sinw_lay_R_Freq_vert_lay);

        sinw_right_chan_cobox = new QComboBox(sinw_tab);
        sinw_right_chan_cobox->setObjectName(QStringLiteral("sinw_right_chan_cobox"));

        sinw_lay_right->addWidget(sinw_right_chan_cobox);

        sinw_right_sampform_cobox = new QComboBox(sinw_tab);
        sinw_right_sampform_cobox->setObjectName(QStringLiteral("sinw_right_sampform_cobox"));

        sinw_lay_right->addWidget(sinw_right_sampform_cobox);

        sinw_right_time_edit = new QLineEdit(sinw_tab);
        sinw_right_time_edit->setObjectName(QStringLiteral("sinw_right_time_edit"));

        sinw_lay_right->addWidget(sinw_right_time_edit);


        sinw_tab_hor_lay_parent->addLayout(sinw_lay_right);

        sinw_tab_hor_lay_parent->setStretch(0, 1);
        sinw_tab_hor_lay_parent->setStretch(1, 1);

        verticalLayout_4->addLayout(sinw_tab_hor_lay_parent);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        sine_play_wave_button = new QPushButton(sinw_tab);
        sine_play_wave_button->setObjectName(QStringLiteral("sine_play_wave_button"));

        horizontalLayout_2->addWidget(sine_play_wave_button);

        sine_save_as_button = new QPushButton(sinw_tab);
        sine_save_as_button->setObjectName(QStringLiteral("sine_save_as_button"));

        horizontalLayout_2->addWidget(sine_save_as_button);


        verticalLayout_4->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);

        tabWidget->addTab(sinw_tab, QString());
        saww_tab = new QWidget();
        saww_tab->setObjectName(QStringLiteral("saww_tab"));
        tabWidget->addTab(saww_tab, QString());

        verticalLayout->addWidget(tabWidget);


        horizontalLayout->addLayout(verticalLayout);

        MainW->setCentralWidget(centralWidget);

        retranslateUi(MainW);

        tabWidget->setCurrentIndex(0);
        sinw_right_chan_cobox->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainW);
    } // setupUi

    void retranslateUi(QMainWindow *MainW)
    {
        MainW->setWindowTitle(QApplication::translate("MainW", "MainW", 0));
        LoadNPlayFile_Button->setText(QApplication::translate("MainW", "Load And Play Sound File", 0));
        sinw_left_output_file_lab->setText(QApplication::translate("MainW", "Output file name", 0));
        sinw_left_samp_rate_lab->setText(QApplication::translate("MainW", "Sample Rate", 0));
        sinw_left_LNR_freq->setText(QApplication::translate("MainW", "Left and Right Frequence [Hz]", 0));
        sinw_left_channels_lab->setText(QApplication::translate("MainW", "Channels", 0));
        sinw_left_sample_format_lab->setText(QApplication::translate("MainW", "Sample Format", 0));
        sinw_left_play_time_lab->setText(QApplication::translate("MainW", "Time (second)", 0));
        sinw_right_outputfile_edit->setText(QApplication::translate("MainW", "sine.wav", 0));
        sinw_right_samp_rate_edit->setText(QApplication::translate("MainW", "44100", 0));
        sinw_lay_R_LFREQ_edit->setText(QApplication::translate("MainW", "466", 0));
        sinw_lay_R_LFREQ_edit->setPlaceholderText(QString());
        sinw_lay_R_RFREQ_edit->setText(QApplication::translate("MainW", "344", 0));
        sinw_right_chan_cobox->clear();
        sinw_right_chan_cobox->insertItems(0, QStringList()
         << QApplication::translate("MainW", "1", 0)
         << QApplication::translate("MainW", "2", 0)
        );
        sinw_right_sampform_cobox->clear();
        sinw_right_sampform_cobox->insertItems(0, QStringList()
         << QApplication::translate("MainW", "Float32", 0)
         << QApplication::translate("MainW", "Int32", 0)
         << QApplication::translate("MainW", "Int24", 0)
         << QApplication::translate("MainW", "Int16", 0)
         << QApplication::translate("MainW", "Int8", 0)
         << QApplication::translate("MainW", "UInt8", 0)
        );
        sinw_right_time_edit->setText(QApplication::translate("MainW", "4", 0));
        sine_play_wave_button->setText(QApplication::translate("MainW", "Play Wave", 0));
        sine_save_as_button->setText(QApplication::translate("MainW", "Save as...", 0));
        tabWidget->setTabText(tabWidget->indexOf(sinw_tab), QApplication::translate("MainW", "Sin wave", 0));
        tabWidget->setTabText(tabWidget->indexOf(saww_tab), QApplication::translate("MainW", "Saw wave", 0));
    } // retranslateUi

};

namespace Ui {
    class MainW: public Ui_MainW {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINW_H
