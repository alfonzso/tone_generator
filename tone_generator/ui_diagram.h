/********************************************************************************
** Form generated from reading UI file 'diagram.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAGRAM_H
#define UI_DIAGRAM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_diagramform
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;

    void setupUi(QWidget *diagramform)
    {
        if (diagramform->objectName().isEmpty())
            diagramform->setObjectName(QStringLiteral("diagramform"));
        diagramform->resize(400, 300);
        verticalLayout_2 = new QVBoxLayout(diagramform);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));

        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(diagramform);

        QMetaObject::connectSlotsByName(diagramform);
    } // setupUi

    void retranslateUi(QWidget *diagramform)
    {
        diagramform->setWindowTitle(QApplication::translate("diagramform", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class diagramform: public Ui_diagramform {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAGRAM_H
