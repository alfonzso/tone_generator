#ifndef MAINW_H
#define MAINW_H

#include <c_functions.h>

#include <QMainWindow>
#include <QFileDialog>
#include <qdebug.h>

namespace Ui {
class MainW;
class diagramform;
}


class MainW : public QMainWindow
{
    Q_OBJECT

public:

    QWidget * newwindow;

    explicit MainW(QWidget *parent = 0);
    ~MainW();

    int GetSampleFormatFromGui();
    paTestData * Load_Gui_Datas();

    int MakeSinToFile(paTestData *dat);

private slots:
    int on_LoadNPlayFile_Button_clicked();

    void on_sinw_right_chan_cobox_currentIndexChanged(const QString &arg1);

    void on_sine_play_wave_button_clicked();

    void on_sine_save_as_button_clicked();

private:

/*
    int SAMPLE_RATE;
    int SAMPLE_COUNT;
    int CHANNELS;
    float AMPLITUDE;
    float LEFT_FREQ;
    float RIGHT_FREQ;
    int TIME;*/

    Ui::MainW *ui;
    Ui::diagramform * lol;
};

#endif // MAINW_H
